#!/bin/bash

# Define the Docker network
network_name="cassandra_cluster_network"
echo "Creating Docker network: $network_name"
docker network create $network_name

# Cassandra Docker image
cassandra_image="cassandra:latest"

# Cluster configuration
seed_node="cassandra-node-1"

# Launching Cassandra node
docker run -d \
	--name cassandra-node -p 9042:9042 \
	--network $network_name \
	$cassandra_image


echo "Cassandra node has been launched."

# Keep the window open
echo "Press enter to exit"
read