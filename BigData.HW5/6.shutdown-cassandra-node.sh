#!/bin/bash

# Define the Docker network and container name (should match run-cassandra-node.sh)
network_name="cassandra_cluster_network"
containers=("cassandra-node")

# Stop and remove Cassandra containers
for container in "${containers[@]}"; do
    echo "Stopping and removing container: $container"
    docker stop $container
    docker rm $container
done

# Remove the Docker network
echo "Removing Docker network: $network_name"
docker network rm $network_name

echo "Shutdown completed."

# Keep the window open
echo "Press enter to exit"
read