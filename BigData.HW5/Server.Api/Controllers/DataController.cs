using Cassandra;
using Microsoft.AspNetCore.Mvc;

namespace Server.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DataController : ControllerBase
    {
        private readonly Cassandra.ISession _session;

        public DataController()
        {
            var cluster = Cluster
                .Builder()
                .AddContactPoint("cassandra-node") // localhost
                .WithPort(9042)
                .Build();
            _session = cluster.Connect("hw5_paitak");
        }

        [HttpGet("products/{productId}/reviews")]
        public async Task<Cassandra.RowSet> GetAllReviewsForSpecifiedProductId([FromRoute] string productId)
        {
            var statement = _session.Prepare("SELECT * FROM reviews_by_product WHERE product_id = ?");
            var boundStatement = statement.Bind(productId);
            return await _session.ExecuteAsync(boundStatement).ConfigureAwait(false);
        }

        [HttpGet("products/{productId}/reviews/by-star-rating/{starRating}")]
        public async Task<Cassandra.RowSet> GetAllReviewsForSpecifiedProductIdByStarRating([FromRoute] string productId, [FromRoute] int starRating)
        {
            var statement = _session.Prepare("SELECT * FROM reviews_by_product_and_rating WHERE product_id = ? AND star_rating = ?");
            var boundStatement = statement.Bind(productId, starRating);
            return await _session.ExecuteAsync(boundStatement).ConfigureAwait(false);
        }

        [HttpGet("customers/{customerId}/reviews")]
        public async Task<Cassandra.RowSet> GetAllReviewsForSpecifiedCustomerId([FromRoute] string customerId)
        {
            var statement = _session.Prepare("SELECT * FROM reviews_by_customer WHERE customer_id = ?");
            var boundStatement = statement.Bind(customerId);
            return await _session.ExecuteAsync(boundStatement).ConfigureAwait(false);
        }

        [HttpGet("reviews/most-reviewed")]
        public async Task<Cassandra.RowSet> GetMostReviewedItemsForGivenPeriodOfTime([FromQuery] DateTime period, [FromQuery] int n = 10)
        {
            var statement = _session.Prepare("SELECT * FROM most_reviewed_products_by_period WHERE period = ? LIMIT ?");
            var boundStatement = statement.Bind(new LocalDate(period.Year, period.Month, period.Day), n);
            return await _session.ExecuteAsync(boundStatement).ConfigureAwait(false);
        }

        [HttpGet("customers/most-productive")]
        public async Task<Cassandra.RowSet> GetMostProductiveCustomersForGivenPeriodOfTime([FromQuery] DateTime period, [FromQuery] int n = 10)
        {
            var statement = _session.Prepare("SELECT * FROM most_productive_customers_by_period WHERE period = ? LIMIT ?");
            var boundStatement = statement.Bind(new LocalDate(period.Year, period.Month, period.Day), n);
            return await _session.ExecuteAsync(boundStatement).ConfigureAwait(false);
        }

        [HttpGet("customers/most-haters")]
        public async Task<Cassandra.RowSet> GetMostProductiveHatersForGivenPeriodOfTime([FromQuery] DateTime period, [FromQuery] int n = 10)
        {
            var statement = _session.Prepare("SELECT * FROM most_productive_haters_by_period WHERE period = ? LIMIT ?");
            var boundStatement = statement.Bind(new LocalDate(period.Year, period.Month, period.Day), n);
            return await _session.ExecuteAsync(boundStatement).ConfigureAwait(false);
        }

        [HttpGet("customers/most-backers")]
        public async Task<Cassandra.RowSet> GetMostProductiveBackersForGivenPeriodOfTime([FromQuery] DateTime period, [FromQuery] int n = 10)
        {
            var statement = _session.Prepare("SELECT * FROM most_productive_backers_by_period WHERE period = ? LIMIT ?");
            var boundStatement = statement.Bind(new LocalDate(period.Year, period.Month, period.Day), n);
            return await _session.ExecuteAsync(boundStatement).ConfigureAwait(false);
        }
    }
}