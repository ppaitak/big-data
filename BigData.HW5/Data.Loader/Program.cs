﻿using Cassandra;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using System.Globalization;

var cluster = Cluster
    .Builder()
    .AddContactPoint("cassandra-node") // localhost
    .WithPort(9042)
    .Build();
var session = cluster.Connect("hw5_paitak");

string filePath = "/app/amazon_reviews.csv"; //"C:\\Users\\pavlo\\Downloads\\amazon_reviews.csv";
List<Review> reviews = ReadExcelFile(filePath);

foreach (var review in reviews)
{
    var statement1 = session.Prepare("INSERT INTO reviews_by_product (product_id, review_id) VALUES (?, ?)");
    var boundStatement1 = statement1.Bind(review.ProductId, review.ReviewId);
    session.Execute(boundStatement1);

    var statement2 = session.Prepare("INSERT INTO reviews_by_product_and_rating (product_id, star_rating, review_id) VALUES (?, ?, ?)");
    var boundStatement2 = statement2.Bind(review.ProductId, review.StarRating, review.ReviewId);
    session.Execute(boundStatement2);

    var statement3 = session.Prepare("INSERT INTO reviews_by_customer (customer_id, review_id) VALUES (?, ?)");
    var boundStatement3 = statement3.Bind(review.CustomerId, review.ReviewId);
    session.Execute(boundStatement3);
}

var reviewCountsByDailyPeriod = reviews
.GroupBy(review => new { ProductId = review.ProductId, Period = new LocalDate(review.ReviewDate.Year, review.ReviewDate.Month, review.ReviewDate.Day) })
.Select(group => new
{
ProductId = group.Key.ProductId,
Period = group.Key.Period,
Count = group.Count()
});

foreach (var item in reviewCountsByDailyPeriod)
{
    var statement = session.Prepare("INSERT INTO most_reviewed_products_by_period (period, product_id, review_count) VALUES (?, ?, ?)");
    var boundStatement = statement.Bind(item.Period, item.ProductId, item.Count);
    session.Execute(boundStatement);
}

var productiveCustomersByPeriod = reviews
    .Where(review => review.VerifiedPurchase == true) // Filter for verified purchases
    .GroupBy(review => new { CustomerId = review.CustomerId, Period = new LocalDate(review.ReviewDate.Year, review.ReviewDate.Month, review.ReviewDate.Day) })
    .Select(group => new
    {
        CustomerId = group.Key.CustomerId,
        Period = group.Key.Period,
        Count = group.Count()
    });

foreach (var item in productiveCustomersByPeriod)
{
    var statement = session.Prepare("INSERT INTO most_productive_customers_by_period (period, customer_id, review_count) VALUES (?, ?, ?)");
    var boundStatement = statement.Bind(item.Period, item.CustomerId, item.Count);
    session.Execute(boundStatement);
}

var hatersByPeriod = reviews
    .Where(review => review.StarRating == 1 || review.StarRating == 2) // Filter for 1 or 2-star ratings
    .GroupBy(review => new { CustomerId = review.CustomerId, Period = new LocalDate(review.ReviewDate.Year, review.ReviewDate.Month, review.ReviewDate.Day) })
    .Select(group => new
    {
        CustomerId = group.Key.CustomerId,
        Period = group.Key.Period,
        LowStarReviewCount = group.Count()
    });

foreach (var item in hatersByPeriod)
{
    var statement = session.Prepare("INSERT INTO most_productive_haters_by_period (period, customer_id, low_star_review_count) VALUES (?, ?, ?)");
    var boundStatement = statement.Bind(item.Period, item.CustomerId, item.LowStarReviewCount);
    session.Execute(boundStatement);
}

var backersByPeriod = reviews
    .Where(review => review.StarRating == 4 || review.StarRating == 5) // Filter for 4 or 5-star ratings
    .GroupBy(review => new { CustomerId = review.CustomerId, Period = new LocalDate(review.ReviewDate.Year, review.ReviewDate.Month, review.ReviewDate.Day) })
    .Select(group => new
    {
        CustomerId = group.Key.CustomerId,
        Period = group.Key.Period,
        HighStarReviewCount = group.Count()
    });

foreach (var item in backersByPeriod)
{
    var statement = session.Prepare("INSERT INTO most_productive_backers_by_period (period, customer_id, high_star_review_count) VALUES (?, ?, ?)");
    var boundStatement = statement.Bind(item.Period, item.CustomerId, item.HighStarReviewCount);
    session.Execute(boundStatement);
}

Console.WriteLine("Finished");

List<Review> ReadExcelFile(string filePath, int maxRows = 1_000) // You can change at put any number here, but be aware that it can take for a while :)
{
    FileInfo fileInfo = new FileInfo(filePath);
    List<Review> reviews = new List<Review>();

    var config = new CsvConfiguration(CultureInfo.InvariantCulture)
    {
        HasHeaderRecord = true,
        MissingFieldFound = null
    };

    using (var reader = new StreamReader(filePath))
    using (var csv = new CsvReader(reader, config))
    {
        csv.Read();
        csv.ReadHeader();
        while (csv.Read() && reviews.Count < maxRows)
        {
            var record = csv.GetRecord<Review>();
            reviews.Add(record);
        }
    }

    return reviews;
}

public class Review
{
    [Name("marketplace")]
    public string Marketplace { get; set; }

    [Name("customer_id")]
    public string CustomerId { get; set; }

    [Name("review_id")]
    public string ReviewId { get; set; }

    [Name("product_id")]
    public string ProductId { get; set; }

    [Name("product_parent")]
    public string ProductParent { get; set; }

    [Name("product_title")]
    public string ProductTitle { get; set; }

    [Name("product_category")]
    public string ProductCategory { get; set; }

    [Name("star_rating")]
    public int StarRating { get; set; }

    [Name("helpful_votes")]
    public int HelpfulVotes { get; set; }

    [Name("total_votes")]
    public int TotalVotes { get; set; }

    [Name("vine")]
    public string Vine { get; set; }

    [Name("verified_purchase")]
    public bool VerifiedPurchase { get; set; }

    [Name("review_headline")]
    public string ReviewHeadline { get; set; }

    [Name("review_body")]
    public string ReviewBody { get; set; }

    [Name("review_date")]
    public DateTime ReviewDate { get; set; }
}