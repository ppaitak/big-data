#!/bin/bash

# Define the Docker network and container names (should match build-and-run-server-api.sh)
CONTAINERS=("server-api")

# Stop and remove containers
for container in "${CONTAINERS[@]}"; do
    echo "Stopping and removing container: $container"
    docker stop $container
    docker rm $container
done

echo "Shutdown completed."

# Keep the window open
echo "Press enter to exit"
read