#!/bin/bash

# Define your image name
IMAGE_NAME="data-loader"

# Define the Docker network
NETWORK_NAME="cassandra_cluster_network"

# Define the path to your Dockerfile
# Assuming the Dockerfile is in a subdirectory named 'DockerFolder' next to the script
DOCKERFILE_PATH="./Data.Loader"

# Step 1: Build the Docker image
docker build -t $IMAGE_NAME -f $DOCKERFILE_PATH/Dockerfile .

# Start the container
docker run -d --network $NETWORK_NAME --name data-loader $IMAGE_NAME

# Step 2: Keep the window open
echo "Press enter to exit"
docker ps
read