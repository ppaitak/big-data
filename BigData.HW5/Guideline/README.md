# BigData

Look of root folder for this HW.

![root-folder.png](root-folder.png)

## Guideline
0. The csv file must be placed here with the same name, bec git lab does not allow to save so big files.
   ![img_8.png](img_8.png)

1. Run `1.run-cassandra-node.sh`.
   ![run-cassandra-node.png](run-cassandra-node.png)
   ![run-cassandra-node-result.png](run-cassandra-node-result.png)
   Run `1.1.schema.cql` using a Docker command to run cqlsh in a Docker container.
   `docker run --rm --network=cassandra_cluster_network -v C:\{path-to-your-scripts}\big-data\BigData.HW5:/scripts cassandra cqlsh cassandra-node -f /scripts/1.1schema.cql`
      * --rm: Removes the container after the script execution is complete.
      * -v C:\{path-to-your-scripts}\big-data\BigData.HW5:/scripts: Mounts the directory containing your script into the container at /scripts. 
      * cassandra: Uses the Cassandra Docker image. 
      * cqlsh cassandra-node: Runs cqlsh and connects to cassandra-node. 
      * -f /scripts/create.cql: Executes the commands in your script file.

   ![1.1.schema-result.png](1.1.schema-result.png)

   Verify everything by using the cqlsh tool to execute some simple queries by using a Docker container to run cqlsh. 
   * `docker run -it --rm --network=cassandra_cluster_network cassandra cqlsh cassandra-node`
   * `DESCRIBE KEYSPACES;`
   * `DESCRIBE KEYSPACE hw5_paitak;`
   * `DESCRIBE TABLE hw5_paitak.reviews_by_product;`
   * `SELECT * FROM hw5_paitak.reviews_by_product;`
   * `...`

   ![verify-result-part-1.png](verify-result-part-1.png)
   ![verify-result-part-2.png](verify-result-part-2.png)
   ![verify-result-part-3.png](verify-result-part-3.png)
   ![verify-result-part-3.png](verify-result-part-4.png)
   ![verify-result-part-4.png](verify-result-part-5.png)
   ![verify-result-part-6.png](verify-result-part-6.png)
   ![verify-result-part-7.png](verify-result-part-7.png)
   ![verify-result-part-8.png](verify-result-part-8.png)
2. Run `2.build-and-run-data-loader.sh`.
   ![2.build-and-run-data-loader.png](2.build-and-run-data-loader.png)
   ![2.build-and-run-data-loader-result.png](2.build-and-run-data-loader-result.png)

   Verify everything by using the cqlsh tool to execute some simple queries by using a Docker container to run cqlsh.
   * `docker run -it --rm --network=cassandra_cluster_network cassandra cqlsh cassandra-node`
   * `SELECT * FROM hw5_paitak.reviews_by_product LIMIT 10;`
   * `...`
     ![verify-2.png](verify-2.png)
3. Run `3.build-and-run-data-loader.sh`.
   ![3.build-and-run-data-loader.png](3.build-and-run-data-loader.png)
   ![3.build-and-run-data-loader-result.png](3.build-and-run-data-loader-result.png)
   ![3.build-and-run-data-loader-swagger.png](3.build-and-run-data-loader-swagger.png)

   Verify each endpoint:

   GET   /Data/products/{productId}/reviews
   ![img.png](img.png)
   GET   /Data/products/{productId}/reviews/by-star-rating/{starRating}
   ![img_1.png](img_1.png)
   GET   /Data/customers/{customerId}/reviews
   ![img_2.png](img_2.png)
   GET   /Data/reviews/most-reviewed
   ![img_3.png](img_3.png)
   GET   /Data/customers/most-productive
   ![img_4.png](img_4.png)
   GET   /Data/customers/most-haters
   ![img_5.png](img_5.png)
   GET   /Data/customers/most-backers
   ![img_6.png](img_6.png)
4. Clean everything using 4-6 scripts. Some why it creates a second image for cassandra, so make sure you run `6.shutdown-cassandra-node.sh` 2 times.
   ![img_7.png](img_7.png)