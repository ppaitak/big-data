#!/bin/bash

# Define your image name
IMAGE_NAME="server-api"

# Define the Docker network
NETWORK_NAME="cassandra_cluster_network"

# Define the path to your Dockerfile
# Assuming the Dockerfile is in a subdirectory named 'DockerFolder' next to the script
DOCKERFILE_PATH="./Server.Api"

# Step 1: Build the Docker image
docker build -t $IMAGE_NAME -f $DOCKERFILE_PATH/Dockerfile .

# Start the container
docker run -d -p 8081:8081 -p 8080:8080 --network $NETWORK_NAME --name server-api $IMAGE_NAME

# Step 2: Keep the window open
echo "Press enter to exit"
docker ps
read