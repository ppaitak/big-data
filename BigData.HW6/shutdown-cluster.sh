#!/bin/bash

# Define network name
NETWORK_NAME="kafka-network"

# Stop and remove Kafka container
docker stop kafka
docker rm kafka

# Stop and remove Zookeeper container
docker stop zookeeper
docker rm zookeeper

# Remove the Docker network
docker network rm "$NETWORK_NAME"

echo "Kafka cluster and containers have been stopped and removed."

echo "Press enter to exit"
docker ps
read