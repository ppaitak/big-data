# BigData



## Guideline

1. Run `docker-compose.yml`.
    ![img.png](img.png)
    ![img_1.png](img_1.png)
2. Create the topic "test-topic" with 3 partitions:
    `docker exec -it kafka kafka-topics --create --topic test-topic --partitions 3 --replication-factor 1 --bootstrap-server kafka:9092`
    ![img_2.png](img_2.png)
3. Write 4-5 messages to the "test-topic" using the Kafka producer:
    `docker exec -it kafka kafka-console-producer --topic test-topic --broker-list kafka:9092`
    ![img_3.png](img_3.png)
4. Open a new terminal and use the Kafka consumer to read messages from the "test-topic": 
    `docker exec -it kafka kafka-console-consumer --topic test-topic --bootstrap-server kafka:9092 --from-beginning`
    ![img_4.png](img_4.png)
5. Run `shutdown-cluster.sh`.
    ![img_5.png](img_5.png)

