#!/bin/bash

# Define the Docker network and container names (should match run-cluster.sh)
network_name="cassandra_cluster_network"
containers=("cassandra-node-1" "cassandra-node-2" "cassandra-node-3")

# Stop and remove Cassandra containers
for container in "${containers[@]}"; do
    echo "Stopping and removing container: $container"
    docker stop $container
    docker rm $container
done

# Remove the Docker network
echo "Removing Docker network: $network_name"
docker network rm $network_name

echo "Cassandra cluster shutdown completed."

# Keep the window open
echo "Press enter to exit"
read