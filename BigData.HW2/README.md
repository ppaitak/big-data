# BigData



## Guideline

1. Run `run-cluster.sh`. Make sure all nodes started successfully, if not just restart one of them manually.
   ![img.png](img.png)
2. Run `schema.cql` using a Docker command to run cqlsh in a Docker container.
   * `docker run --rm --network=cassandra_cluster_network -v C:\{your-path}\big-data\BigData.HW2:/scripts cassandra cqlsh cassandra-node-1 -f /scripts/schema.cql`
      * --rm: Removes the container after the script execution is complete.
      * -v C:\{path-to-your-scripts}:/scripts: Mounts the directory containing your script into the container at /scripts. 
      * cassandra: Uses the Cassandra Docker image. 
      * cqlsh cassandra-node-1: Runs cqlsh and connects to cassandra-node-1. 
      * -f /scripts/create.cql: Executes the commands in your script file.
3. Run `insert.cql` using a Docker command to run cqlsh in a Docker container.
   * `docker run --rm --network=cassandra_cluster_network -v C:\{your-path}\big-data\BigData.HW2:/scripts cassandra cqlsh cassandra-node-1 -f /scripts/insert.cql`
4. Verify everything by using the cqlsh tool to execute some simple queries by using a Docker container to run cqlsh. 
   * `docker run -it --rm --network=cassandra_cluster_network cassandra cqlsh cassandra-node-1`
   * `docker run -it --rm --network=cassandra_cluster_network cassandra cqlsh cassandra-node-2`
   * `docker run -it --rm --network=cassandra_cluster_network cassandra cqlsh cassandra-node-3`
   * `DESCRIBE KEYSPACES;`
   * `DESCRIBE KEYSPACE hw2_paitak;`
   * `DESCRIBE TABLE hw2_paitak.favorite_songs;`
   * `DESCRIBE TABLE hw2_paitak.favorite_movies;`
   * `SELECT * FROM hw2_paitak.favorite_songs;`
   * `SELECT * FROM hw2_paitak.favorite_movies;`

   ![img_1.png](img_1.png)
5. Run `shutdown-cluster.sh`.
   ![img_2.png](img_2.png)

