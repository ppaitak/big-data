#!/bin/bash

# Define the Docker network
network_name="cassandra_cluster_network"
echo "Creating Docker network: $network_name"
docker network create $network_name

# Cassandra Docker image
cassandra_image="cassandra:latest"

# Cluster configuration
cluster_name="MyCassandraCluster"
seed_node="cassandra-node-1"

# Launching Cassandra nodes
for i in 1 2 3; do
    echo "Launching Cassandra node $i"
    docker run -d \
        --name cassandra-node-$i \
        --network $network_name \
        -e CASSANDRA_CLUSTER_NAME=$cluster_name \
        -e CASSANDRA_SEEDS=$seed_node \
        $cassandra_image
done

echo "Cassandra cluster with 3 nodes has been launched."

# Keep the window open
echo "Press enter to exit"
read