﻿using Confluent.Kafka;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using System.Globalization;
using System.Text.Json;

namespace Data.Loader.Publisher;

public class Program
{
    public static void Main(string[] args)
    {
        Thread.Sleep(5000); // Adjust as needed

        string filePath = "/app/sample.csv";
        List<Tweet> tweets = ReadCsvFile(filePath);

        var config = new ProducerConfig
        {
            BootstrapServers = "kafka:9092",
            ClientId = "my_client_id",
            Acks = Acks.None, // Set the acknowledgment mode as needed
        };

        using (var producer = new ProducerBuilder<Null, string>(config).Build())
        {
            // Read tweets from the CSV file, replace date/time, and send as separate messages.
            // You can use CsvHelper to read the CSV file as shown in the previous example.

            foreach (var tweet in tweets) // Replace 'tweetsFromCsv' with your list of Tweet objects
            {
                // Replace the date/time with the current one
                tweet.CreatedAt = DateTime.UtcNow.ToString();

                // Serialize the tweet object to a JSON string (or any other format you prefer)
                var tweetJson = JsonSerializer.Serialize(tweet);

                // Create a message to send to the Kafka topic
                var message = new Message<Null, string> { Value = tweetJson };

                // Publish the message to the Kafka topic
                producer.Produce("tweets-topic", message);

                // Add a delay to control the sending speed (e.g., 10-15 messages per second)
                Thread.Sleep(1000); // Adjust as needed
            }

            // Wait for any outstanding messages to be delivered and delivery reports to be received.
            producer.Flush(TimeSpan.FromSeconds(10));
        }

        Console.WriteLine("Finished.");
    }

    private static List<Tweet> ReadCsvFile(string filePath, int maxRows = 94)
    {
        List<Tweet> tweets = new List<Tweet>();

        var config = new CsvConfiguration(CultureInfo.InvariantCulture)
        {
            HasHeaderRecord = true,
            MissingFieldFound = null
        };

        using (var reader = new StreamReader(filePath))
        using (var csv = new CsvReader(reader, config))
        {
            csv.Read();
            csv.ReadHeader();
            while (csv.Read() && tweets.Count < maxRows)
            {
                var record = csv.GetRecord<Tweet>();
                tweets.Add(record);
            }
        }

        return tweets;
    }
}

public class Tweet
{
    [Name("tweet_id")]
    public string TweetId { get; set; }

    [Name("author_id")]
    public string AuthorId { get; set; }

    [Name("inbound")]
    public bool Inbound { get; set; }

    [Name("created_at")]
    public string CreatedAt { get; set; }

    [Name("text")]
    public string Text { get; set; }

    [Name("response_tweet_id")]
    public string ResponseTweetId { get; set; }

    [Name("in_response_to_tweet_id")]
    public string InResponseToTweetId { get; set; }
}
