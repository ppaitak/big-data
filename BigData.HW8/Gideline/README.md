# BigData



## Guideline

1. Run `docker-compose.yml` up.
    ![img.png](img.png)
    ![img_1.png](img_1.png)
2. Check data-subscriber output folder and files in docker container: 
    ![img_2.png](img_2.png)
    ![img_3.png](img_3.png)
3. Run `docker-compose.yml` down.
    ![img_4.png](img_4.png)
    ![img_5.png](img_5.png)

