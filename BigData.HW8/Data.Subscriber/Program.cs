﻿using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Text.Json;
using Confluent.Kafka;
using CsvHelper;

namespace Data.Subscriber
{
    class Program
    {
        private const string KafkaBootstrapServers = "kafka:9092";
        private const string KafkaTopic = "tweets-topic";
        private const string OutputDirectory = "output";

        static async Task Main(string[] args)
        {
            // Create the output directory if it doesn't exist
            Directory.CreateDirectory(OutputDirectory);

            var config = new ConsumerConfig
            {
                GroupId = "data-subscriber-group",
                BootstrapServers = KafkaBootstrapServers,
                AutoOffsetReset = AutoOffsetReset.Earliest
            };

            using var consumer = new ConsumerBuilder<Ignore, string>(config).Build();

            consumer.Subscribe(KafkaTopic);

            Console.WriteLine("Data.Subscriber is running...");

            while (true)
            {
                try
                {
                    var consumeResult = consumer.Consume();

                    Console.WriteLine(consumeResult.Message.Value);
                    var tweet = DeserializeKafkaMessage(consumeResult.Message.Value);

                    // Process the tweet and extract author_id, created_at, and text fields
                    var authorId = tweet.AuthorId;
                    var createdAt = DateTime.Parse(tweet.CreatedAt);
                    var text = tweet.Text;

                    // Determine the file name based on the created_at timestamp
                    var fileName = $"tweets_{createdAt:dd_MM_yyyy_HH_mm}.csv";

                    // Create or append to the CSV file and write the data
                    WriteDataToCsvFile(fileName, authorId, createdAt, text);

                    Console.WriteLine($"Message received and saved to {fileName}");
                }
                catch (ConsumeException e)
                {
                    Console.WriteLine($"Error consuming message: {e.Error.Reason}");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error: {e}");
                }
            }
        }

        private static Tweet DeserializeKafkaMessage(string message)
        {
            Tweet tweet = JsonSerializer.Deserialize<Tweet>(message)!;
            return tweet;
        }

        private static void WriteDataToCsvFile(string fileName, string authorId, DateTime createdAt, string text)
        {
            var filePath = Path.Combine(OutputDirectory, fileName);

            using (var writer = new StreamWriter(filePath, true, Encoding.UTF8))
            using (var csv = new CsvWriter(writer, new CsvHelper.Configuration.CsvConfiguration(System.Globalization.CultureInfo.InvariantCulture)))
            {
                csv.WriteRecord(new { AuthorId = authorId, CreatedAt = createdAt, Text = text });
                csv.NextRecord();
            }
        }
    }

    // Define your Tweet class to match the structure of your Kafka messages
    public class Tweet
    {
        public string AuthorId { get; set; }

        public string CreatedAt { get; set; }

        public string Text { get; set; }
    }
}
