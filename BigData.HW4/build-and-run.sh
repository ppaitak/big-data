#!/bin/bash

# Define your image name
IMAGE_NAME="bd-hw4"

# Define the Docker network
NETWORK_NAME="local"

# Define the path to your Dockerfile
# Assuming the Dockerfile is in a subdirectory named 'DockerFolder' next to the script
DOCKERFILE_PATH="."

# Step 1: Build the Docker image
docker build -t $IMAGE_NAME -f $DOCKERFILE_PATH/Dockerfile .

# Create the network
docker network create $NETWORK_NAME

# Start the container
docker run -d -p 8081:8081 -p 8080:8080 --network local --name e_commerce $IMAGE_NAME

# Step 2: Keep the window open
echo "Press enter to exit"
docker ps
read