#!/bin/bash

# Define the Docker network and container names (should match build-and-run.sh)
NETWORK_NAME="local"
CONTAINERS=("e_commerce")

# Stop and remove containers
for container in "${CONTAINERS[@]}"; do
    echo "Stopping and removing container: $container"
    docker stop $container
    docker rm $container
done

# Remove the Docker network
echo "Removing Docker network: $NETWORK_NAME"
docker network rm $NETWORK_NAME

echo "shutdown completed."

# Keep the window open
echo "Press enter to exit"
read