using BigData.HW4.Models;
using Microsoft.AspNetCore.OData;
using Microsoft.OData.ModelBuilder;

namespace BigData.HW4;

internal class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        var modelBuilder = new ODataConventionModelBuilder();
        modelBuilder.EntitySet<Book>("Books");
        builder.Services
            .AddControllers()
            .AddOData(
                options => options
                .Select()
                .Filter()
                .OrderBy()
                .Expand()
                .Count()
                .SetMaxTop(null)
                .AddRouteComponents("odata", modelBuilder.GetEdmModel()));
        builder.Services.AddControllers();

        var app = builder.Build();
        app.UseRouting();
        app.UseEndpoints(endpoints => endpoints.MapControllers());
        app.Run();
    }
}