﻿namespace BigData.HW4.Models;

public class Book
{
    public Guid Id { get; init; } = Guid.NewGuid();
    public string Title { get; init; }
    public string Author { get; init; }
    public int Year { get; init; }
    public string Genre { get; init; }
}
