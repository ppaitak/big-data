﻿using BigData.HW4.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;

namespace BigData.HW4.Controllers;

public class BooksController : ODataController
{
    private static List<Book> _books = new List<Book>
    {
        new Book
        {
            Title = "The Great Gatsby",
            Author = "F. Scott Fitzgerald",
            Year = 1925,
            Genre = "Novel",
        },
        new Book
        {
            Title = "1984",
            Author = "George Orwell",
            Year = 1949,
            Genre = "Dystopian"
        },
        new Book
        {
            Title = "To Kill a Mockingbird",
            Author = "Harper Lee",
            Year = 1960,
            Genre = "Novel"
        },
        new Book
        {
            Title = "A Brief History of Time",
            Author = "Stephen Hawking",
            Year = 1988,
            Genre = "Science"
        },
        new Book
        {
            Title = "The Lord of the Rings",
            Author = "J.R.R. Tolkien",
            Year = 1954,
            Genre = "Fantasy"
        },
        new Book
        {
            Title = "Pride and Prejudice",
            Author = "Jane Austen",
            Year = 1813,
            Genre = "Novel"
        },
        new Book
        {
            Title = "The Catcher in the Rye",
            Author = "J.D. Salinger",
            Year = 1951,
            Genre = "Novel"
        }
    };

    [HttpGet]
    [EnableQuery]
    public ActionResult<IReadOnlyCollection<Book>> Get()
    {
        return Ok(_books);
    }

    [HttpGet("{key}")]
    [EnableQuery]
    public ActionResult<Book> Get([FromRoute] Guid key)
    {
        var item = _books.SingleOrDefault(d => d.Id.Equals(key));

        if (item == null)
        {
            return NotFound();
        }

        return Ok(item);
    }

    [EnableQuery]
    public ActionResult<Book> Post([FromBody] Book book)
    {
        _books.Add(book);
        return Created(book);
    }
}
