# BigData



## Guideline

1. Run `docker-compose.yml` up.
    ![img.png](img.png)
    ![img_1.png](img_1.png)
2. Open a new terminal and use the Kafka consumer to read messages from the "tweets-topic": 
    `docker exec -it kafka kafka-console-consumer --topic tweets-topic --bootstrap-server kafka:9092 --from-beginning`
    ![img_2.png](img_2.png)
    ![img_3.png](img_3.png)
3. Run `docker-compose.yml` down.
    ![img_4.png](img_4.png)

