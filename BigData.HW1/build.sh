#!/bin/bash

# Define your image name
IMAGE_NAME="big-data-hw1"

# Define the path to your Dockerfile
# Assuming the Dockerfile is in a subdirectory named 'DockerFolder' next to the script
DOCKERFILE_PATH="./BigData.HW1"

# Step 1: Build the Docker image
docker build -t $IMAGE_NAME -f $DOCKERFILE_PATH/Dockerfile .

# Step 2: Keep the window open
echo "Press enter to exit"
read