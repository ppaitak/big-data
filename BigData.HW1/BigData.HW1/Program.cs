﻿Console.CursorVisible = false;  // Hide the cursor for better display

while (true)
{
    DateTime now = DateTime.Now;
    Console.SetCursorPosition(0, 0);  // Set cursor to the beginning of the first line
    Console.WriteLine("Date Time Now: " + now.ToString("yyyy-MM-dd HH:mm:ss"));  // Format the date and time

    if (Console.CursorTop == 1)  // Check if the second line is not yet written
    {
        Console.WriteLine("Pavlo Paitak");  // Write "Pavlo Paitk" on the second line
    }

    Thread.Sleep(1000);  // Wait for one second before updating
}
